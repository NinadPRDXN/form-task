/* Author: 

*/
var list = [], data = document.getElementById('list'), myModal = document.getElementById('myModal');
document.getElementById('btn-submit').addEventListener("click", myFunction);
document.getElementById('cancel').addEventListener("click", cancel);
document.getElementById('term-condition-input').addEventListener("click", confirmation);
document.getElementById('sortByName').addEventListener("click", sort_by_name);
document.getElementById('sortByLastName').addEventListener("click", sort_by_lastname);
document.getElementById('okay').addEventListener("click", okay);


//For Submission of new value
function myFunction() {
    var fName = document.getElementById('first-name-input').value;
    var lName = document.getElementById('last-name-input').value;
    var address = document.getElementById('address-input').value;
    var txt = " ";

    if (document.getElementById("male").checked) {
        var gender = document.getElementById("male").value;
    }
    else {
        var gender = document.getElementById("female").value;
    }

    var letters = /^[A-Za-z]+$/;
    if (!letters.test(fName)) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Firstname should be in alphabetical form and should not be empty.';
    }
    else if (!letters.test(lName)) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Lastname should be in alphabetical form and should not be empty.';
    }
    else if (!document.getElementById("male").checked && !document.getElementById("female").checked) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Please select the gender.';
    }
    else if (document.getElementById('address-input').value.length == 0) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Address field should not empty';
    }
    else if (!document.getElementById("term-condition-input").checked) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Please accept the Terms and Condition to proceed further.';
    }
    else {
        data.style.display = 'block';
        document.getElementById('table-body').innerHTML = ' ';
    
        list.push({'firstName': fName, 'lastName': lName, 'gender': gender, 'address': address});

        list.forEach(listFunction);

        function listFunction(data, index) {
            txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="edit(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';
        }
        document.getElementById('table-body').innerHTML = txt;
    }

}

//For reseting all the inputs
function cancel() {
    document.getElementById('first-name-input').value = '';
    document.getElementById('last-name-input').value = '';
    document.getElementById('address-input').value = '';
    document.getElementById('male').checked = false;
    document.getElementById('female').checked = false;
    document.getElementById('term-condition-input').checked = false;
    document.getElementById('btn').innerHTML = '<button onclick="myFunction()">submit</button><button onclick="cancel()">cancel</button>';
}

//okay button on model
function okay() {
    myModal.style.display = 'none';
}

//For Retrieving the value for updation
function edit(edit_index) {
    document.getElementById('table-body').innerHTML = ' ';
    txt = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        if (index == edit_index) {
            
        }
        else {
            txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="edit(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

            document.getElementById('table-body').innerHTML = txt;
        }
    }
    document.getElementById('first-name-input').value = list[edit_index].firstName;
    document.getElementById('last-name-input').value = list[edit_index].lastName;
    document.getElementById('address-input').value = list[edit_index].address;
    if (list[edit_index].gender == 'male') {
        document.getElementById('male').checked = true;
    }
    else {
        document.getElementById('female').checked = true;
    }
    document.getElementById('btn').innerHTML = '<button onclick="update(' + edit_index + ')">edit</button><button onclick="cancel()">cancel</button>';
}

//For updating the values after editing
function update(index) {
    var fName = document.getElementById('first-name-input').value;
    var lName = document.getElementById('last-name-input').value;
    var address = document.getElementById('address-input').value;
    var txt = " ";

    if (document.getElementById("male").checked) {
        var gender = document.getElementById("male").value;
    }
    else {
        var gender = document.getElementById("female").value;
    }

    var letters = /^[A-Za-z]+$/;
    if (!letters.test(fName)) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Firstname should be in alphabetical form and should not be empty.';
    }
    else if (!letters.test(lName)) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Lastname should be in alphabetical form and should not be empty.';
    }
    else if (!document.getElementById("male").checked && !document.getElementById("female").checked) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Please select the gender.';
    }
    else if (document.getElementById('address-input').value.length == 0) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Address field should not empty';
    }
    else if (!document.getElementById("term-condition-input").checked) {
        myModal.style.display = 'block';
        document.getElementById('alert_message').innerHTML = 'Please accept the Terms and Condition to proceed further.';
    }
    else {
        list[index].firstName = fName;
        list[index].lastName = lName;
        list[index].gender = gender;
        list[index].address = address;

        document.getElementById('table-body').innerHTML = ' ';
        list.forEach(listFunction);

        function listFunction(data, index) {
            txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="myFunction(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

            document.getElementById('table-body').innerHTML = txt;
        }
    }
}

//For deleting particular element from an array
function remove(index) {
    list.splice(index, 1);
    txt = ' ';
    document.getElementById('table-body').innerHTML = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="edit(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

        document.getElementById('table-body').innerHTML = txt;
    }

    document.getElementById('btn').innerHTML = '<button onclick="myFunction()">submit</button><button onclick="cancel()">cancel</button>';

    console.log(list);
}

//For Terms and Condition acceptance
function confirmation() {
    var userConfirmation = confirm("Are you sure that you have read all terms and conditions?");
    if (userConfirmation == true) {
        document.getElementById('term-condition-input').checked = true;
    }
    else {
        document.getElementById('term-condition-input').checked = false;
    }
}

//For sorting the values by name
function sort_by_name() {
    list.sort((a, b) => (a.firstName.toLowerCase() > b.firstName.toLowerCase()) ? 1 : -1);
    txt = ' ';
    document.getElementById('table-body').innerHTML = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="myFunction(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

        document.getElementById('table-body').innerHTML = txt;
    }
}

//By sorting the value by lastname
function sort_by_lastname() {
    list.sort((a, b) => (a.lastName.toLowerCase() > b.lastName.toLowerCase()) ? 1 : -1);
    txt = ' ';
    document.getElementById('table-body').innerHTML = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="myFunction(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

        document.getElementById('table-body').innerHTML = txt;
    }
}






















